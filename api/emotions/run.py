import os
import pandas as pd
import numpy as np
import random
import re
import nltk
import sys
from nltk.tokenize import word_tokenize
from nltk.tokenize import wordpunct_tokenize
# from nltk.tokenize import sent_tokenize (Tokenization)
from nltk.probability import FreqDist
from nltk.metrics import ConfusionMatrix

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from sklearn import metrics
from sklearn.model_selection import GridSearchCV
from joblib import dump, load


sentence = str(sys.argv[1])
#print(sentence)

def _remove_url(data):
    ls = []
    words = ''
    regexp1 = re.compile('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')
    regexp2 = re.compile('www?.(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')
    
    for line in data:
        urls = regexp1.findall(line)

        for u in urls:
            line = line.replace(u, ' ')

        urls = regexp2.findall(line)

        for u in urls:
            line = line.replace(u, ' ')
            
        ls.append(line)
    return ls

def _remove_regex(data, regex_pattern):
    ls = []
    words = ''
    
    for line in data:
        matches = re.finditer(regex_pattern, line)
        
        for m in matches: 
            line = re.sub(m.group().strip(), '', line)

        ls.append(line)

    return ls

def _tokenize_text(data):
    ls = []

    for line in data:
        tokens = wordpunct_tokenize(line)
        ls.append(tokens)

    return ls

def _apply_standardization(tokens, std_list):
    ls = []

    for tk_line in tokens:
        new_tokens = []
        
        for word in tk_line:
            if word.lower() in std_list:
                word = std_list[word.lower()]
                
            new_tokens.append(word) 
            
        ls.append(new_tokens)

    return ls

nltk.download('stopwords')
nltk_stopwords = nltk.corpus.stopwords.words('portuguese')

def _remove_stopwords(tokens, stopword_list):
    ls = []

    for tk_line in tokens:
        new_tokens = []
        
        for word in tk_line:
            if word.lower() not in stopword_list:
                new_tokens.append(word) 
            
        ls.append(new_tokens)
        
    return ls

nltk.download('rslp')

def _apply_stemmer(tokens):
    ls = []
    stemmer = nltk.stem.RSLPStemmer()

    for tk_line in tokens:
        new_tokens = []
        
        for word in tk_line:
            word = str(stemmer.stem(word))
            new_tokens.append(word) 
            
        ls.append(new_tokens)
        
    return ls

def _untokenize_text(tokens):
    ls = []

    for tk_line in tokens:
        new_line = ''
        
        for word in tk_line:
            new_line += word + ' '
            
        ls.append(new_line)
        
    return ls

emoticon_list = {
    ':))': 'positive_emoticon', 
    ':)': 'positive_emoticon', 
    ':D': 'positive_emoticon', 
    ':(': 'negative_emoticon', 
    ':((': 'negative_emoticon', 
    '8)': 'neutral_emoticon'
}

def _replace_emoticons(data, emoticon_list):
    ls = []

    for line in data:
        for exp in emoticon_list:
            line = line.replace(exp, emoticon_list[exp])

        ls.append(line)

    return ls

std_list = {
    'eh': 'é', 
    'vc': 'você', 
    'vcs': 'vocês',
    'tb': 'também', 
    'tbm': 'também', 
    'obg': 'obrigado', 
    'gnt': 'gente', 
    'q': 'que', 
    'n': 'não', 
    'cmg': 'comigo', 
    'p': 'para', 
    'ta': 'está', 
    'to': 'estou', 
    'vdd': 'verdade'
}
#vectorizer = CountVectorizer()
vectorizer = load('vectorizer.joblib')
tfidf_transformer = load("transformer.joblib")
def transformIntoPrediction(new_text):
    X_new = new_text
    # Remove urls from text (http(s), www)
    X_new = _remove_url(X_new)
    # Remove hashtags
    regex_pattern = '#[\w]*'
    X_new = _remove_regex(X_new, regex_pattern)
    # Remove notations
    regex_pattern = '@[\w]*'
    X_new = _remove_regex(X_new, regex_pattern)
    # Replace emoticons ":)) :) :D :(" to positive_emoticon or negative_emoticon or neutral_emoticon
    X_new = _replace_emoticons(X_new, emoticon_list)
    # Tokenize text
    X_new_tokens = _tokenize_text(X_new)
    # Object Standardization
    X_new_tokens = _apply_standardization(X_new_tokens, std_list)
    # remove stopwords
    X_new_tokens = _remove_stopwords(X_new_tokens, nltk_stopwords)

    # Stemming (dimensionality reduction)
    X_new_tokens = _apply_stemmer(X_new_tokens)

    # Dataset preparation
    # Untokenize text (transform tokenized text into string list)
    X_new = _untokenize_text(X_new_tokens)

    # Text to Features
    # Feature extraction from text 
    # Method: bag of words
    X_new_vect = vectorizer.transform(X_new)

    #print(X_new_vect.shape)
    # TF-IDF: Term Frequency - Inverse Document Frequency
    # use the transform(...) method to transform count-matrix to a tf-idf representation.
    X_new_tfidf = tfidf_transformer.transform(X_new_vect)

    return X_new_tfidf

model = load('model.joblib')
data = transformIntoPrediction([sentence])

print(str(model.predict(data)))
sys.stdout.flush()