import NLPController from "./NLPController";

const fsRegular = require('fs');
const fs = fsRegular.promises;
const util = require('util');
const exec = util.promisify(require('child_process').exec);

export default class RecognitionController {
    constructor(
        private nlpController : NLPController,
        public modelPath : string, 
        public lmPath : string,
        public triePath : string,
        public alphabetPath : string
    ) {
        this.checkForFiles();
    }

    async checkForFiles() {
        let files = [this.modelPath, this.lmPath, this.triePath, this.alphabetPath];
        for (let file of files) {
            try {
                await fs.access(file);
            } catch (error) {
                console.log("Couldn't find " + file);
            }
        }
    }

    async transcribe(sound: Buffer[]) {
        try {
            console.log("Got sound");
            let tempFile = __dirname + '/temp.wav';
            // Saving sound to filesystem
            await fs.writeFile(tempFile, sound);

            const { error, stdout, stderr } = await exec('deepspeech' +
                ' --model ' + this.modelPath +
                ' --lm ' + this.lmPath +
                ' --trie ' + this.triePath +
                ' --alphabet ' + this.alphabetPath +
                ' --audio ' + tempFile
            );
            await fs.unlink(tempFile);

            console.log("<==========================>");
            console.log("Got Audio data");
            console.log(stdout);
            console.log("<==========================>");
            
            await this.nlpController.parseText(stdout);
        } catch (error) {
            console.log('Error transcribing sound (' + error + ')');
        }
    }

}