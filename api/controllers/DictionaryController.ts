const fsRegular = require('fs');
const fs = fsRegular.promises;

export default class DictionaryController{

    private data: string[];

    constructor() {
        this.data = [];
        this.loadData();
    }

    async loadData() {

        let dictionaryLocation = __dirname + '/../dictionary.txt';
        try {
            await fs.access(dictionaryLocation);
        }catch (e) {
            console.error('[DictionaryController] [loadData] Dictionary file not found, creating one...');

            await fs.writeFile(dictionaryLocation, '');
        }

        this.data = (await fs.readFile(dictionaryLocation)).toString().split('\n');
    }

    removeDupplicates() {
        let seen: any = {} ;
        this.data = this.data.filter(function (item) {
            return seen.hasOwnProperty(item) ? false : (seen[item] = true);
        });
    }

    sort() {
        this.data = this.data.sort();
    }

    async addNewWord(word){
        if (this.data[word]){
            throw new Error('This word already exists');
        }
        this.data.push(word);
        await this.sort();
        await this.save();
    }

    async save() {
        let dataToWrite = this.data.join('\n');
        await fs.writeFile( __dirname +'/../dictionary.txt', dataToWrite);
    }
}
