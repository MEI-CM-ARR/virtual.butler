import textToSpeech from "@google-cloud/text-to-speech";

export default class TextToSpeechAPIController {

    private client;
    private readonly languageCode: string;

    constructor() {
        this.client = new textToSpeech.TextToSpeechClient();
        this.languageCode = 'pt-PT';
    }

    async getWordSound(word, voice) {
        const request = {
            input: {text: word},
            // Select the language and SSML Voice Gender (optional)
            voice: {
                languageCode: this.languageCode,
                ssmlGender: 'NEUTRAL',
                name: this.languageCode + '-Standard-' + voice
            },
            // Select the type of audio encoding
            audioConfig: {audioEncoding: 'LINEAR16'},
        };

        let [response] = await this.client.synthesizeSpeech(request);
        return response;
    }

}