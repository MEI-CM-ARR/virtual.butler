import IoServerController from "./IoServerController";
import * as fastify from 'fastify';

export default class OSGIServerController {

    constructor(public fastify: fastify.FastifyInstance, public ioServerController: IoServerController) {

        fastify.post('/speak',async (request, reply) => {
            let spoken = await ioServerController.speak('');
            reply.code(spoken?200:500).send();
        });

        fastify.post('/move',async (request, reply) => {
            let moved = await ioServerController.move(request.body.locationID);
            reply.code(moved?200:500).send();
        });
    }
}