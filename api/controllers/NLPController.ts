import SynthesizerController from "./SynthesizerController";

const fsRegular = require('fs');
const fs = fsRegular.promises;
const util = require('util');
const exec = util.promisify(require('child_process').exec);

export default class NLPController {

    constructor(private synthetizer: SynthesizerController) {}

    async parseText(text: string) {
        try {
            console.log("Parsing text in the future");
            console.log(text.includes('tempo'));
            text = text.toLowerCase();
            switch (true) {
                case text.includes('hora'):
                    await this.synthetizer.speak(new Date().toDateString());
                    break;
                case text.includes('tempo'):
                    console.log("A dizer o tempo");
                    await this.synthetizer.speak("Hoje está sol");
                    break;
                case text.includes('como estas'):
                    let answers = ['Estou muito bem', 'Hoje estou muito feliz', 'Hoje sinto-me útil'];
                    await this.synthetizer.speak(answers[Math.floor(Math.random() * answers.length)]);
                    break;
                default:
                    //let unknowAnswers = ['Não percebi, poderia repetir', 'Ainda não percebo esse pedido'];
                    //await this.synthetizer.speak(unknowAnswers[Math.floor(Math.random() * unknowAnswers.length)]);
                    console.log("Não conheço");
                    return;
            }
        } catch (error) {
            console.log('Error parsing text (' + error + ')');
        }
    }
}