import DictionaryController from './DictionaryController';
import TextToSpeechAPIController from './TextToSpeechAPIController';

const wavFileInfo = require('wav-file-info');

const fsRegular = require('fs');
const fs = fsRegular.promises;

const util = require('util');
const exec = util.promisify(require('child_process').exec);

export default class SynthesizerController {

    private readonly voices: string[];
    private readonly currentVoice: string;
    private dictionary: DictionaryController;
    private readonly inLearningMode: boolean;

    constructor() {
        this.voices = ['a', 'b', 'c', 'd'];
        this.currentVoice = this.voices[0];
        this.dictionary = new DictionaryController();
        this.inLearningMode = true;
    }

    async loadWordsSoundFiles(words) {
        let folder = 'audio/voice-' + this.currentVoice + '/';
        let files = [];
        for (let word of words) {
            word = word.toLocaleLowerCase();
            let filename = folder + word + '.wav';
            let fileExits = true;
            try {
                await fs.access(filename, fsRegular.F_OK)
            } catch (e) {
                fileExits = false;
            }

            if (!fileExits) {
                await this.learnNewWord(word);
            }

            files.push(filename);
        }
        return files;
    }

    async learnNewWord(word) {
        if (!this.inLearningMode) {
            return;
        }

        let textToSpeechAPI = new TextToSpeechAPIController();
        let response = await textToSpeechAPI.getWordSound(word, this.currentVoice);
        let filename = __dirname + '/../audio/voice-' + this.currentVoice + '/' + word + '.wav';
        await fs.writeFile(filename, response.audioContent, 'binary');
        await this.dictionary.addNewWord(word);

        return filename;
    }

    async speak(text) {

        let words = SynthesizerController.parseTextToWords(text);
        let files = await this.loadWordsSoundFiles(words);

        for (let file of files) {
            let duration = await this.getDuration(file);
            exec('aplay ' + file);
            await this.sleep(duration * 1000 * 0.80);
        }
    }

    static parseTextToWords(text) {
        // text = text.replace(/,/g, '').replace(/\./g, '').replace(/\!/g, '').replace(/\?/g, '');
        text = text.replace(/\./g, '');
        return text.split(' ');

    }

    async textToAudio(text): Promise<Buffer[]> {

        let words = SynthesizerController.parseTextToWords(text);

        let files = await this.loadWordsSoundFiles(words);

        let result = [];
        for (let file of files) {
            result.push({
                bytes: await fs.readFile(file),
                duration: await this.getDuration(file)
            })
        }

        return result;
    }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }


    getDuration(file): Promise<number> {
        return new Promise((resolve, reject) => {
            wavFileInfo.infoByFilename(file, function (err, info) {
                if (err) {
                    reject();
                    return;
                }
                resolve(info.duration);
            });
        });
    }

}