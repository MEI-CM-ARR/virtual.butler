import * as socketio from "socket.io";
import * as fastify from 'fastify'
import SynthesizerController from "./SynthesizerController";
import RecognitionController from "./RecognitionController";
import * as SocketIO from "socket.io";

export default class IoServerController {

    private currentActiveSocket: socketio.Socket;

    private activeClients: any[] = [];

    constructor(private fastify:fastify.FastifyInstance, private synthesizer: SynthesizerController, private recognizer: RecognitionController) {
        
        let io = socketio(fastify.server);
        io.on("connection", (socket: socketio.Socket) => {
            console.log("a user connected");
            if (this.activeClients.length === 0) {
                this.currentActiveSocket = socket;
            } else {
                socket.emit('deactivate', {});
            }
            this.activeClients.push({
                id: this.activeClients.length,
                socket : socket
            });
            socket.on('listen', async (data) => { await this.handleAudio(data)});
        });
        
    }

    async speak(text:string):Promise<boolean> {
        if (!this.currentActiveSocket) {
            return false;
        }

        let bytesOfAudio: Buffer[] = await this.synthesizer.textToAudio(text);

        this.currentActiveSocket.emit('speak', bytesOfAudio);
        return true;
    }

    async move(locationId:number): Promise<boolean> {
        this.currentActiveSocket.emit('deactivate', {});
        this.currentActiveSocket = this.activeClients.find((sock: any) => {
            return sock.id == locationId;
        }).socket;
        this.currentActiveSocket.emit('activate', {});

        return true;
    }

    async handleAudio(data) {
        await this.recognizer.transcribe(data);
    }
}




