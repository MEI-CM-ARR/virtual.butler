import IoServerController from "./controllers/IoServerController";
import OSGIServerController from "./controllers/OSGIServerController";
import SynthesizerController from "./controllers/SynthesizerController";
import RecognitionController from "./controllers/RecognitionController";
import * as fastify from 'fastify';

import {Server, IncomingMessage, ServerResponse} from 'http';
import NLPController from "./controllers/NLPController";

const configFile = require('./config.json');
const server: fastify.FastifyInstance<Server, IncomingMessage, ServerResponse> = fastify({});
//console.log(configFile);

let synthesizer = new SynthesizerController();
//console.log(synthesizer);
let nlpController = new NLPController(synthesizer);
let recognitionController = new RecognitionController(nlpController, configFile.recognition.model, configFile.recognition.lm, configFile.recognition.trie, configFile.recognition.alphabet);
let ioServerController = new IoServerController(server, synthesizer, recognitionController);
let osgiServerController = new OSGIServerController(server, ioServerController);

server.listen(configFile.server.port, configFile.server.ip, (err: Error, address: String) => {
    if (err) {
        console.error(err);
    }
    console.log(`Listen on ${address}`);
});




