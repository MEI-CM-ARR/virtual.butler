"use strict";
/* CODIGO PARA O DETETOR DE EMOCOES

document.querySelector('#abc').addEventListener('click', start)
function start() {
    let text = document.querySelector('#sentence').value
    console.log(text)
    var process = spawn(
        'python3',
        [
            "run.py",
            text
        ]
    );

    process.stdout.on('data', function (data) {
        console.log(data.toString());
    });

    process.stderr.on('data', function (data) {
        console.log(data.toString());
    });
}
*/
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
exports.__esModule = true;
var IoServerController_1 = require("./controllers/IoServerController");
var SynthesizerController_1 = require("./controllers/SynthesizerController");
var RecognitionController_1 = require("./controllers/RecognitionController");
var fastify = require("fastify");
var NLPController_1 = require("./controllers/NLPController");
var configFile = require('./config.json');
var server = fastify({});
console.log(configFile);
var synthesizer = new SynthesizerController_1["default"]();
var nlpController = new NLPController_1["default"]();
var recognitionController = new RecognitionController_1["default"](nlpController, configFile.recognition.model, configFile.recognition.lm, configFile.recognition.trie, configFile.recognition.alphabet);
var ioServerController = new IoServerController_1["default"](server, synthesizer, recognitionController);
//let osgiServerController = new OSGIServerController(server, ioServerController);
server.listen(configFile.server.port, configFile.server.ip, function (err, address) {
    if (err) {
        console.error(err);
    }
    console.log("Listen on " + address);
});
(function () { return __awaiter(_this, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/];
    });
}); })();
//# sourceMappingURL=index.js.map