import SocketController from "./SocketController";

let mic = require('mic');
let fs = require('fs');
const FILENAME = 'recorded.wav';
export default class MicController {
    private socketController : SocketController;

    constructor(socketController) {
        this.socketController = socketController;

        let micInstance = mic({
            rate: '16000',
            channels: '2',
            debug: false,
            fileType: 'wav'
        });
        this.registerEvents(micInstance);
        micInstance.start();
    }

    private registerEvents(micInstance) {
        let micInputStream = micInstance.getAudioStream();

        let outputFileStream = fs.WriteStream(FILENAME);

        micInputStream.pipe(outputFileStream);

        // TODO: Make this usefull
        micInputStream.on('data', (data) => {
            //console.log("Recieved Input Stream: " + data.length);
        });
        micInputStream.on('silence', () => {
            //console.log("Got SIGNAL silence");
        });
        // TODO: Ate aqui

        micInputStream.on('error', (err) => {
            console.log("Error in Input Stream: " + err);
        });

        micInputStream.on('startComplete',  () => {
            setTimeout(async () => {
                console.log("Pausing");
                micInstance.stop();
                await this.socketController.sendAudio(FILENAME);
            }, 5000);
        });

        micInputStream.on('stopComplete', () => {
            setTimeout(() => {

                micInstance = mic({
                    rate: '16000',
                    channels: '2',
                    debug: false,
                    device: 'hw:0,0',
                    fileType: 'wav'
                });
                this.registerEvents(micInstance);

                micInstance.start();
            }, 100);
        });
    }
}