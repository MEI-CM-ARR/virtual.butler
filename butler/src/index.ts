// Modules to control application life and create native browser window
import SocketController from "./SocketController";
import MicController from "./MicController";

const electron = require('electron');

const { app, BrowserWindow} = electron;
app.commandLine.appendSwitch('autoplay-policy', 'no-user-gesture-required');
//app.commandLine.appendSwitch('enable-speech-dispatcher');
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
const url = require('url');

const path = require('path');
const fsRegular = require('fs');
const fs = fsRegular.promises;
const config = require('../config.json');

function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true
        },
        frame: false
    });

    mainWindow.webContents.on('did-finish-load', () => {
        let socketController = new SocketController(config.api.ip, config.api.port,  mainWindow.webContents);
        let micController = new MicController(socketController);
    });

    // and load the index.html of the app.
    mainWindow.loadFile( app.getAppPath() + '/frontend/index.html');

    // Open the DevTools.
    //mainWindow.webContents.openDevTools()
    
    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    });

    

    mainWindow.setFullScreen(true);
    mainWindow.setMenuBarVisibility(false);
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});