import IpcMain = Electron.IpcMain;

const io = require('socket.io-client');
import Electron = require('electron');

import IpcRenderer = Electron.IpcRenderer;
import WebContents = Electron.WebContents;

const stream = require('stream');
const Speaker = require('speaker');

const bufferConcat = require('array-buffer-concat');

const THRESHOLD = 20;

const fs = require('fs').promises;

export default class SocketController {

    private socket;

    constructor(ip, port, private webContents: WebContents) {

        this.socket = io('http://' + ip + ":" + port);

        this.socket.on('connect', async () => {
            this.socket.on('speak', async (files: any[]) => {
                let finalSound = [];
                for (let i = 0; i < files.length; i++) {
                    files[i].bytes = SocketController.removeWavHeader(files[i].bytes);

                    if (i != 0) {
                        files[i].bytes = await SocketController.trimBufferStart(files[i].bytes);
                    }

                    // if (files.length - 1 != i) {
                    //     // files[i].bytes = await SocketController.trimBufferEnd(files[i].bytes);
                    // }

                    finalSound.push(files[i].bytes);
                }

                let finalBuffer = Buffer.concat(finalSound);
                // await fs.writeFile('test.wav', finalBuffer);
                this.playAudioFromBuffer(finalBuffer);
            });

            this.socket.on('activate', () => {
                webContents.executeJavaScript("document.getElementById('babylon-canvas').style.visibility = 'visible';");
            });

            this.socket.on('deactivate', () => {
                webContents.executeJavaScript("document.getElementById('babylon-canvas').style.visibility = 'hidden';");
            });
        });
    }

    static removeWavHeader(bytes) {
        return bytes.slice(44, bytes.length);

    }

    static trimBufferStart(array) {
        return new Promise((resolve) => {
            if (array[0] > THRESHOLD) {
                return resolve(array);
            }
            array = array.slice(1, array.length);

            process.nextTick(async () => {
                resolve(await this.trimBufferStart(array));
            });
        });
    }

    static async trimBufferEnd(array) {
        return new Promise((resolve) => {
            let lastIndex = array.length - 1;

            if (array[lastIndex] > THRESHOLD) {
               return resolve(array);
            }

            array = array.slice(0, array.length - 1);


            setTimeout(async() => {
                resolve(await this.trimBufferEnd(array));
            }, 0);


        });
    }


    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    playAudioFromBuffer(fileContents) {
        const speaker = new Speaker({
            channels: 1,          // 2 channels
            bitDepth: 16,         // 16-bit samples
            sampleRate: 24000     // 44,100 Hz sample rate
        });
        let bufferStream = new stream.PassThrough();
        bufferStream.end(fileContents);
        bufferStream.pipe(speaker);
    }

    async sendAudio(filename) {
        let wavFileContent = await fs.readFile(filename);
        this.socket.emit('listen', wavFileContent);
    }

}