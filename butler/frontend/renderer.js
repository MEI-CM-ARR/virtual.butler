// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const spawn = require("child_process").spawn;

const electron = require('electron');
const {ipcRenderer} = electron;

let canvas = document.getElementById('babylon-canvas');
var engine = new BABYLON.Engine(canvas, true);
BABYLON.SceneLoader.ShowLoadingScreen = false;
BABYLON.SceneLoader.Load("", "test.babylon", engine, function (scene) {

    //as this .babylon example hasn't camera in it, we have to create one
    //var camera = new BABYLON.ArcRotateCamera("Camera", -Math.PI/2, Math.PI/2, 0, BABYLON.Vector3.Zero(), scene);
    //var camera = new BABYLON.ArcRotateCamera("Camera", -Math.PI / 2, Math.PI / 2, 0, new BABYLON.Vector3(0, 0.08, 0), scene);
    var camera = new BABYLON.FreeCamera("MainCam", new BABYLON.Vector3(0,-0.2, -3), scene);
    camera.attachControl(canvas, false);
    camera.radius = 2;
    camera.useBouncingBehavior = true;
    //camera.rotationOffset = 10;

    scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);
    scene.ambientColor = new BABYLON.Color3.Gray;



    let rotationSpeed = 0.001;
    let iteration = 1;
    let currentStage = 0;
    let cycleDone = false;
    scene.registerBeforeRender(() => {
        //camera.position.x -= moveSpeed;


        switch (currentStage) {
            case 0:
                // rodar no x vai para cima e para baixo
                camera.rotation = new BABYLON.Vector3( camera.rotation.x - rotationSpeed, camera.rotation.y - rotationSpeed, 0);
                iteration++;

                if (iteration > 200) {
                    cycleDone = true;
                    iteration = -iteration;
                    rotationSpeed = -rotationSpeed;
                }

                if (cycleDone && iteration === 0) {
                    currentStage++;
                    rotationSpeed = 0.001;
                    iteration = 1;
                }


                break;
            case 1:
                camera.rotation = new BABYLON.Vector3(camera.rotation.x - rotationSpeed, camera.rotation.y + rotationSpeed, 0);
                iteration++;

                if (iteration > 200) {
                    cycleDone = true;
                    iteration = -iteration;
                    rotationSpeed = -rotationSpeed;
                }

                if (cycleDone && iteration === 0) {
                    rotationSpeed = 0.001;
                    currentStage = 0;
                    iteration = 1;
                }

                break;
            default:
                console.log("Magic happened");
        }







    });
    engine.runRenderLoop(function () {
        scene.render();
    });

// Window configuration
    engine.switchFullscreen(true);

    window.addEventListener("resize", function () {
        engine.resize();
    });

    canvas.setAttribute('height', window.outerHeight);
    canvas.setAttribute('width', window.outerWidth);



    //scene.autoClear = false;
});


